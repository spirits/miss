// components/star/star.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    "starlist":[
      {
        'path': "/static/images/h1.jpg"
      },
      {
        'path': "/static/images/h2.jpg"
      },
      {
        'path': "/static/images/h3.jpg"
      },
      {
        'path': "/static/images/h4.jpg"
      },
      {
        'path': "/static/images/h5.jpg"
      },
      {
        'path': "/static/images/h6.jpg"
      },
      {
        'path': "/static/images/h7.jpg"
      },
      {
        'path': "/static/images/h8.jpg"
      },
      {
        'path': "/static/images/h9.jpg"
      },
      {
        'path':"/static/images/h1.jpg"
      },
      {
        'path': "/static/images/h2.jpg"
      },
      {
        'path': "/static/images/h3.jpg"
      },
      {
        'path': "/static/images/h4.jpg"
      },
      {
        'path': "/static/images/h5.jpg"
      },
      {
        'path': "/static/images/h6.jpg"
      },
      {
        'path': "/static/images/h7.jpg"
      },
      {
        'path': "/static/images/h8.jpg"
      },
      {
        'path': "/static/images/h9.jpg"
      }, {
        'path': "/static/images/h1.jpg"
      },
      {
        'path': "/static/images/h2.jpg"
      },
      {
        'path': "/static/images/h3.jpg"
      },
      {
        'path': "/static/images/h4.jpg"
      },
      {
        'path': "/static/images/h5.jpg"
      },
      {
        'path': "/static/images/h6.jpg"
      },
      {
        'path': "/static/images/h7.jpg"
      },
      {
        'path': "/static/images/h8.jpg"
      },
      {
        'path': "/static/images/h9.jpg"
      }
    ],
    "wrapperStyleValue":"",
    "activeNum":13,
    "currentStyle":""
  },

  /**
   * 组件的方法列表
   */
  tranX:0,
  startX:0,
  precent:1,
  leftMax:0,
  preWidth:0,
  methods: {
    swiperTouchstart:function(e){
     var touches = e.touches[0];
     this.startX= touches.clientX;
    },
    swiperTouchmove:function(e){
      var touches = e.touches[0];
      var moveX = touches.clientX;
      var offsetX = (moveX - this.startX);
      var nowTranX = offsetX + this.tranX;
      this.updateViewAnimation(nowTranX);
 
    },
    swiperTouchend:function(e){
      var touches = e.changedTouches[0];
      var moveX = touches.clientX;
      var nowTranX = (moveX - this.startX);
      this.tranX += Math.round(nowTranX / this.preWidth) * this.preWidth;
      this.updateViewAnimation(this.tranX);
      this.startX = moveX;
    },
    updateViewAnimation: function (nowTranX){
        var num = -Math.floor(nowTranX / this.preWidth) + 13;
        if(num <= 4){
          num = 13 - 4 +num;
          this.tranX = 0;
          num = 13;
          this.animation.translateX(nowTranX).translate3d(0).step({ duration:0});
        }else if(num >= 22){
          num = 13 +num - 22;
          nowTranX = (22 - num) * this.preWidth;
          this.tranX = 0;
        }else{
          this.animation.translateX(nowTranX).translate3d(0).step();
        }
        this.setData({ swiperAnmiation: this.animation.export(), activeNum: num});
    }
  },
  animation:null,
  attached: function () { 
    this.precent = 750 / wx.getSystemInfoSync().windowWidth;
    this.preWidth = 100 / this.precent;
    var width = this.preWidth * this.data.starlist.length ;
    this.leftMax = Math.ceil((width - wx.getSystemInfoSync().windowWidth) / (2 * this.preWidth)) * this.preWidth ;
    this.setData({ wrapperStyleValue: "width:" + width + 'px;left:-' + this.leftMax + "px"});
    this.animation = wx.createAnimation({
      transformOrigin: "50% 50%",
      duration: 100,
      timingFunction: "ease",
      delay: 0
    })
    this.startX = 0;
    this.tranX = 0;
  },
 
  
})
