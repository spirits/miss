// components/index-item/index-item.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    datalist:{
      type:Array,
      value:[]
    }
  },
  /**
   * 组件的初始数据
   */
  data: {
  },

  /**
   * 组件的方法列表
   */
  methods: {
    redectPageToPage:function(e){
      var id = e.currentTarget.dataset.id;
      wx.navigateTo({
        url: '/pages/view/view?id='+id,
      })
    }
  }
})
