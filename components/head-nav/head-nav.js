Component({
   properties: {
     list:{
       type:Array,
       value:[]
     },
     current:{
       type: Number,
       value: 0
     }
   },
    data:{
    leftPosition:0
  },
  methods:{
    clickItem: function (e) {
      var target = e.target;
      var position = target.offsetLeft - 100 > 0 ? target.offsetLeft - 100 : 0;
      if (this.activeNum != target.dataset.num){
        this.setData({ activeNum: target.dataset.num, leftPosition: position });
        this.triggerEvent("myclick", this.data.activeNum);
      }
   
    }
  }
  
})