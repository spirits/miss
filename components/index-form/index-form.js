// components/form/form.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    items: [
      { name: 'USA', value: '美国' },
      { name: 'CHN', value: '中国', checked: 'true' },
      { name: 'BRA', value: '巴西' },
      { name: 'JPN', value: '日本' }
    ],
    time: '12:01',
    defaultSize: 'default',
    primarySize: 'default',
    warnSize: 'default',
    disabled: false,
    plain: false,
    loading: false
  },

  /**
   * 组件的方法列表
   */
  methods: {
    setDisabled: function (e) {
      this.setData({
        disabled: !this.data.disabled
      })
    },
    setPlain: function (e) {
      this.setData({
        plain: !this.data.plain
      })
    },
    setLoading: function (e) {
      this.setData({
        loading: !this.data.loading
      })
    },
    subform:function(e){
      console.log(e);
    }
  }
})
