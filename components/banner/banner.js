// components/banner/banner.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    bannerlist:{
      type:Array,
      value:[]
    },
    autoplay:{
      type:Boolean,
      value:true
    },
    circular:{
      type: Boolean,
      value: true
    },
    interval:{
      type:Number,
      value:1000
    },
    duration:{
      type: Number,
      value: 500
    },
    indicatordots:{
      type: Boolean,
      value: true
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
   
  },

  /**
   * 组件的方法列表
   */
  methods: {
    ontouchend:function(){
      this.properties.autoplay= true;
    },
    ontouchstart:function(e){
      this.properties.autoplay = false;
    }
  }
})
