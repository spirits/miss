// components/orgin-audio-item/origin-audio-item.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    audiolist: {
      type: Array,
      value: []
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    playing: -1,
  },

  /**
   * 组件的方法列表
   */
  methods: {
    playOrPause:function(e){
      var playing = e.currentTarget.dataset.playing;
     
      if(this.data.playing != -1){
        var audio = wx.createAudioContext("myaudio" + this.data.playing, this)
        audio.pause();
      }
      this.setData({ playing: playing });
    }
  }
})
