// components/video-item/video-item.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    videolist:{
      type:Array,
      value:[]
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
      playNum:-1
  },

  /**
   * 组件的方法列表
   */
  methods: {
    playVideo:function(e){
     var num = e.currentTarget.dataset.num;
     if (this.data.playNum < 0){
       var videoContext = wx.createVideoContext('index' + num,this);
        videoContext.play();
     }else{
       var videoContext = wx.createVideoContext('index' + this.data.playNum,this);
       videoContext.pause();
       var videoContext = wx.createVideoContext('index' + num,this);
       videoContext.seek(0);
       videoContext.play();
     }
     this.setData({ playNum:num});
    }
  }
})
