// components/audio-item/audio-item.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    audiolist:{
      type:Array,
      value:[]
    }
  },
  /**
   * 组件的初始数据
   */
  data: {
    playing:-1,
  },
  
  /**
   * 组件的方法列表
   */
  audioctx: null,
  methods: {
    playOrPuase:function(e){
       var playing= e.currentTarget.dataset.playing;
       if(playing == this.data.playing){
          playing = -1;
          this.audioctx.pause();
       }else{
         this.audioctx.setSrc(this.properties.audiolist[playing].audio);
         this.audioctx.seek(this.properties.audiolist[playing].time);
         this.audioctx.play();
       }
       this.setData({playing:playing});
       
    },
    playStatus:function(e){
      console.log(e);
    },
    playingStatus:function(e){
      
      this.triggerEvent("playingaudio", { "currentTime": e.detail.currentTime, 'duration': e.detail.duration, "playing": this.data.playing} );
    },
    endaudion:function(){
      this.setData({ playing: -1 });
    }
  },
  attached: function () {
    this.audioctx = wx.createAudioContext('myaudio', this);
  }
})
