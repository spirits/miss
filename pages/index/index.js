//index.js
//获取应用实例
var app = getApp()
Page({
  data: {
    list: ["列表demo", "视频demo", "自定义音频demo", "原始音频demo", "表单demo"],
    motto: 'Hello World',
    userInfo: {},
    current:4,
    indexpage:{
      bannerlist: [
        {
          "path": "/static/images/1.jpg",
          'title': "one"
        },
        {
          "path": "/static/images/2.jpg",
          'title': "two"
        },
        {
          "path": "/static/images/3.jpg",
          'title': "three"
        },
        {
          "path": "/static/images/4.jpg",
          'title': "four"
        },
        {
          "path": "/static/images/5.jpg",
          'title': "five"
        },
        {
          "path": "/static/images/6.jpg",
          'title': "six"
        }
      ],
      datalist: [
        {
          "category": "正在追",
          "list": [
            {
              "src": "/static/images/1.jpg",
              "title": "花谢花开花满天",
              "desc": "千梦千寻"
            },
            {
              "src": "/static/images/2.jpg",
              "title": "王者出击",
              "desc": "陈赫王嘉尔林志玲"
            },
            {
              "src": "/static/images/3.jpg",
              "title": "大宋提刑官",
              "desc": "大牌云集断案"
            },
            {
              "src": "/static/images/4.jpg",
              "title": "海上牧云记",
              "desc": "就不知π的是什么"
            }
          ]
        },
        {
          "category": "热门推荐",
          "list": [
            {
              "src": "/static/images/5.jpg",
              "title": "密战",
              "desc": "郭富城赵丽颖"
            },
            {
              "src": "/static/images/6.jpg",
              "title": "缝纫机乐队",
              "desc": "大鹏乔杉"
            }
          ]
        }
      ]
    },
    videData:{
      loadshow: false,
      videolist: [
        {
          "src": "http://wxsnsdy.tc.qq.com/105/20210/snsdyvideodownload?filekey=30280201010421301f0201690402534804102ca905ce620b1241b726bc41dcff44e00204012882540400&bizid=1023&hy=SH&fileparam=302c020101042530230204136ffd93020457e3c4ff02024ef202031e8d7f02030f42400204045a320a0201000400",
          'image': "/static/images/1.jpg",
          "authors": [
            "江小白", "江小黑", "江小红"
          ]
        },
        {
          "src": "http://wxsnsdy.tc.qq.com/105/20210/snsdyvideodownload?filekey=30280201010421301f0201690402534804102ca905ce620b1241b726bc41dcff44e00204012882540400&bizid=1023&hy=SH&fileparam=302c020101042530230204136ffd93020457e3c4ff02024ef202031e8d7f02030f42400204045a320a0201000400",
          'image': "/static/images/2.jpg",
          "authors": [
            "王二狗", "大内密探", "零零发"
          ]
        },
        {
          "src": "http://wxsnsdy.tc.qq.com/105/20210/snsdyvideodownload?filekey=30280201010421301f0201690402534804102ca905ce620b1241b726bc41dcff44e00204012882540400&bizid=1023&hy=SH&fileparam=302c020101042530230204136ffd93020457e3c4ff02024ef202031e8d7f02030f42400204045a320a0201000400",
          'image': "/static/images/3.jpg",
          "authors": [
            "王二狗", "大内密探", "零零发"
          ]
        },
        {
          "src": "http://wxsnsdy.tc.qq.com/105/20210/snsdyvideodownload?filekey=30280201010421301f0201690402534804102ca905ce620b1241b726bc41dcff44e00204012882540400&bizid=1023&hy=SH&fileparam=302c020101042530230204136ffd93020457e3c4ff02024ef202031e8d7f02030f42400204045a320a0201000400",
          'image': "/static/images/4.jpg",
          "authors": [
            "王二狗", "大内密探", "零零发"
          ]
        },
        {
          "src": "http://wxsnsdy.tc.qq.com/105/20210/snsdyvideodownload?filekey=30280201010421301f0201690402534804102ca905ce620b1241b726bc41dcff44e00204012882540400&bizid=1023&hy=SH&fileparam=302c020101042530230204136ffd93020457e3c4ff02024ef202031e8d7f02030f42400204045a320a0201000400",
          'image': "/static/images/4.jpg",
          "authors": [
            "王二狗", "大内密探", "零零发"
          ]
        }
      ]
    },
    audioData:{
      audiolist:[
        {
          "icon":"/static/images/h1.jpg",
          'audio':"http://up.mcyt.net/md5/53/Mjc3NDg1MDI=_Qq4329912.mp3",
          'title':"带你去旅行",
          'author':"校长",
          "time":"00:00",
          "percent":"0"
        },
        {
          "icon": "/static/images/h2.jpg",
          'audio': "http://up.mcyt.net/md5/53/MjQ3MjA3NTE=_Qq4329912.mp3",
          'title': "我们不一样",
          'author': "大壮",
          "time": "00:00",
          "percent": "0"
        },
        {
          "icon": "/static/images/h3.jpg",
          'audio': "http://up.mcyt.net/md5/19/MTc5NTY3NzAyMw_Qq4329912.mp3",
          'title': "凡人歌",
          'author': "萧敬腾",
          "time": "00:00",
          "percent": "0"
        }
      ]
    }

  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function () {
    console.log('onLoad')
    var that = this
    //调用应用实例的方法获取全局数据
    app.getUserInfo(function(userInfo){
      //更新数据
      that.setData({
        userInfo:userInfo
      })
    })
  },
  mclick:function(e){
    var id = e.detail;
    this.setData({current:id});
  },
  changedCurrent:function(e){
    var now  = e.detail.current;
    this.setData({ current: now });
  },
  loadMore:function (e) {
   
    var data = this.data.videData.videolist;
    var d = [
      {
        "src": "http://wxsnsdy.tc.qq.com/105/20210/snsdyvideodownload?filekey=30280201010421301f0201690402534804102ca905ce620b1241b726bc41dcff44e00204012882540400&bizid=1023&hy=SH&fileparam=302c020101042530230204136ffd93020457e3c4ff02024ef202031e8d7f02030f42400204045a320a0201000400",
        'image': "/static/images/1.jpg",
        "authors": [
          "江小白", "江小黑", "江小红"
        ]
      },
      {
        "src": "http://wxsnsdy.tc.qq.com/105/20210/snsdyvideodownload?filekey=30280201010421301f0201690402534804102ca905ce620b1241b726bc41dcff44e00204012882540400&bizid=1023&hy=SH&fileparam=302c020101042530230204136ffd93020457e3c4ff02024ef202031e8d7f02030f42400204045a320a0201000400",
        'image': "/static/images/2.jpg",
        "authors": [
          "王二狗", "大内密探", "零零发"
        ]
      },
      {
        "src": "http://wxsnsdy.tc.qq.com/105/20210/snsdyvideodownload?filekey=30280201010421301f0201690402534804102ca905ce620b1241b726bc41dcff44e00204012882540400&bizid=1023&hy=SH&fileparam=302c020101042530230204136ffd93020457e3c4ff02024ef202031e8d7f02030f42400204045a320a0201000400",
        'image': "/static/images/3.jpg",
        "authors": [
          "王二狗", "大内密探", "零零发"
        ]
      },
      {
        "src": "http://wxsnsdy.tc.qq.com/105/20210/snsdyvideodownload?filekey=30280201010421301f0201690402534804102ca905ce620b1241b726bc41dcff44e00204012882540400&bizid=1023&hy=SH&fileparam=302c020101042530230204136ffd93020457e3c4ff02024ef202031e8d7f02030f42400204045a320a0201000400",
        'image': "/static/images/4.jpg",
        "authors": [
          "王二狗", "大内密探", "零零发"
        ]
      },
      {
        "src": "http://wxsnsdy.tc.qq.com/105/20210/snsdyvideodownload?filekey=30280201010421301f0201690402534804102ca905ce620b1241b726bc41dcff44e00204012882540400&bizid=1023&hy=SH&fileparam=302c020101042530230204136ffd93020457e3c4ff02024ef202031e8d7f02030f42400204045a320a0201000400",
        'image': "/static/images/4.jpg",
        "authors": [
          "王二狗", "大内密探", "零零发"
        ]
      }];
   
    var newData = d.concat(data);
    var This = this;
    this.setData({ videData: { loadshow: true } });
    setTimeout(function(){
      This.setData({ videData: { videolist: newData, loadshow: false  }});
    },2000);

  },
  paudio:function(e){
    var playing = e.detail.playing;
    var currentTime = e.detail.currentTime;
    var duration = e.detail.duration;
    var percent = Math.ceil(currentTime / duration * 100);
    var time = (parseInt(Math.ceil(currentTime) / 60) < 1 ? "00" : (parseInt(Math.ceil(currentTime) / 60) < 10 ? "0" + parseInt(Math.ceil(currentTime) / 60): parseInt(Math.ceil(currentTime) / 60))) + ":" + ((Math.ceil(currentTime) % 60) < 10 ? "0" + (Math.ceil(currentTime) % 60): (Math.ceil(currentTime) % 60));
    var progress = "audioData.audiolist[" + playing +"].percent";
    var listtime = "audioData.audiolist[" + playing + "].time";
    this.setData({
      [progress]:percent,
      [listtime]:time
    });
  }
})
