// pages/view/view.js
var WxParse = require('../../wxParse/wxParse');
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      var id = options.id;
      var data = '<div><span style="font-size:16px;">在朋友聚会时认识了一个清纯可人的妹子，</span></div><div><span style="font-size:16px;">简直就是理想的初恋型！</span></div><div><span style="font-size:16px;" >于是大胖就开始对别人紧追不舍了，</span></div><div><span style="font-size:16px;" >经常找妹子聊天，约她出去玩儿，</span></div><div><span style="font-size:16px;" >但是迟迟没有表白。</span></div><div><span style="font-size:16px;" >大胖虽看着是个大大咧咧的人。</span></div><div><span style="font-size:16px;" >实际上却是害羞内敛的。</span></div><div><span style="font-size:16px;" >害怕受伤的人。</span></div><div><span style="font-size:16px;" >由于他一直也不知道妹子的一个想法，</span></div><div><span style="font-size:16px;" >过于担忧失落，就放弃错过了。</span></div><div><span style="font-size:16px;" >很久之后听朋友说起，貌似妹子当时非常有意于他，</span></div><div><span style="font-size:16px;" >但是现在早已另有新欢。</span></div><div><span style="font-size:16px;" >大胖也只能感慨时光荏苒了。</span></div><div><span style="font-size:16px;" >像这样的傻事儿肯定很多人也有过的。</span><div style="text-align: center;"><img  src= "http://sitecdn.puamap.com/zixun/uploads/allimg/170317/7-1F31GF224.jpg" > </div><div></div><div><span style="font-size:16px;" >完全不了解妹子的想法。</span></div><div><span style="font-size:16px;" >到底是喜欢还是不喜欢我啊。</span></div><div><span style="font-size:16px;" >现在，我们来聊一聊喜欢你的妹子到底是怎样的表现？</span></div><img alt="有体香的男生代表什么？第1张" src="http://sitecdn.puamap.com/zixun/uploads/allimg/170322/7-1F322163204.jpg">';
      var that = this;
      WxParse.wxParse('content', 'html', data, that,5);
      console.log(this.data);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  wx.showToast({
    title: '加载成功',
  })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})